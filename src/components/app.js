import mdl from 'material-design-lite/material';
import 'linkstate/polyfill';
import { Helmet } from "react-helmet";
import { h, Component } from 'preact';
import { Router } from 'preact-router';
import { route } from 'preact-router';
import Header from './header';
import Nav from './nav';
import Home from '../routes/home';
import Dashboard from '../routes/dashboard';
import Login from '../routes/login';
import Logout from '../routes/logout';
import Profile from '../routes/profile';
import Debug from '../lib/services/Debug';
import Cache from '../lib/services/Cache';
import Footer from '../components/footer';
import Config from '../config';
Debug.setWildcard('app:*');

// import Home from 'async!./home';
// import Profile from 'async!./profile';

export default class App extends Component {
	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => {
		this.currentUrl = e.url;
	};

	render() {
		return (
			<div id="app">
				<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    			
				<Helmet>
                	
            	</Helmet>
				
				<div class="mdl-layout mdl-js-layout">
				  <header class="mdl-layout__header mdl-layout__header--scroll">
				    <div class="mdl-layout__header-row">
				      <span class="mdl-layout-title">{Config.brandName}</span>
				      <div class="mdl-layout-spacer"></div>
				      <Nav/>
				    </div>
				  </header>
				  <div class="mdl-layout__drawer">
				    <span class="mdl-layout-title">{Config.brandName}</span>
				    <Nav/>
				  </div>
				  <main class="mdl-layout__content">
					    
					    <div class="page-content grid-container">
					    
					    <Router onChange={this.handleRoute}>
							<Home default path="/" />
							<Dashboard path="/dashboard" />
							<Login path="/login" />
							<Logout path="/logout" />
							<Profile path="/profile/" user="me" />
							<Profile path="/profile/:user" />
							<Redirect path="/home" to="/" />
						</Router>
					    </div>
					    
					    <Footer/>
					    
				  </main>
				</div>
	
			</div>
		);
	}
}

class Redirect extends Component {
	componentWillMount() {
		route(this.props.to);
	}

	render() {
		return null;
	}
}
