import { h, Component } from 'preact';
import style from './style';
import { Grid, Cell } from 'preact-mdl';
import { Link } from 'preact-router/match';
export default class Footer extends Component {
    render() {
        return (
            <footer class={style.footer+' mdl-mini-footer'}>
                <Grid>
                    <Cell class="mdl-cell--12-col-desktop mdl-cell--middle">
                        <div class="mdl-mini-footer__middle-section">
                            <div class="mdl-logo">
                                <Link class={style.footer__item} href="/monetize-my-livingspace-address">
                                    STORECO
                                </Link>
                            </div>
                            <ul class={style.text+" mdl-mini-footer__link-list"}>
                              <li class={style.footer__item}><a class={style.footer__item} href="#">Privacy & Terms</a></li>
                              <li class={style.footer__item}><a class={style.footer__item} href="#">Legal mentions</a></li>
                              <li class={style.footer__item}><a class={style.footer__item} href="#">Technical support</a></li>
                              
                              <li class={style.footer__item}>
                                <a  class={style.footer__item} href="tel:+06758945">Call us today 06758945</a>
                              </li>
                              
                              
                            </ul>
                        </div>
                    </Cell>
                </Grid>
            </footer>
        )
    }
}
