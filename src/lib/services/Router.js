import { route as preactRoute } from 'preact-router';

export function route() {
    var args = Array.prototype.slice.call(arguments);
    setTimeout(function() {
        preactRoute.apply(preactRoute, args);
    }, 500);
}
